var express = require('express');
var path = require('path');
var mongodb = require('mongodb');
var http = require('http');
var fs = require('fs');
var assert = require('assert');
var ObjectID = require('mongodb').ObjectID;
var bodyParser = require('body-parser');
var methodOverride  = require("method-override");
// var dbConn = mongodb.MongoClient.connect('mongodb://192.168.43.246:27017/phung_db');
// var dbConn = mongodb.MongoClient.connect('mongodb://192.168.1.12:27017/phung_db');
var dbConn = mongodb.MongoClient.connect('mongodb://52.221.191.112:27017/phung_db');

var app = express();
app.set('view engine', 'html');

app.use(bodyParser.urlencoded({extended: false}));
app.use(methodOverride());
app.use(express.static(path.resolve(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.listen(process.env.PORT || 3000, process.env.IP || '0.0.0.0');

app.post('/post-wine', function(req, res) {
    dbConn.then(function(db) {
        delete req.body._id; //For safety reasons
        var wines = db.collection('wines');
        var wine = req.body;
        console.log('Adding wine: ' + JSON.stringify(wine));
        wines.insert(wine, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred'});
            } else {
                console.log('Success: ' , result.ops);
                res.redirect('view-wines');
            }
        });
    });
});

app.get('/view-wines', function(req, res) {
    dbConn.then(function(db){
        db.collection('wines').find({}).toArray().then(function(results){
            data = '<a href="/index.html">Insert New Wine</a>';
            data += '<h1>List Of Wines</h1>';
            data += '<table border="1" style="border-collapse:collapse" cellspacing="5" cellpadding="15">';
            data += '<tr><th>Name</th><th>Year</th><th>Grapes</th><th>Country</th><th>Region</th><th>Desciption</th><th>Picture</th><th colspan="2">Actions</th></tr>';
            results.forEach(function (row) {
                data += '<tr>';
                data += '<td>' + row.name + '</td>';
                data += '<td>' + row.year + '</td>';
                data += '<td>' + row.grapes + '</td>';
                data += '<td>' + row.country + '</td>';
                data += '<td>' + row.region + '</td>';
                data += '<td>' + row.description + '</td>';
                data += '<td>' + row.picture + '</td>';
                data += 
                '<td><form action="/update-wine" method="POST" ><input type="hidden" placeholder="", name="_id", value=' + row._id + '><button type="submit">UPDATE</button></form></td>';
                data += 
                '<td><form action="/deleteproject" method="POST"><input type="hidden" placeholder="", name="_id", value=' + row._id + '><button type="submit">DELETE</button></form></td>';
                data += '</tr>';
            });
            data += '</table>';
            res.writeHead(200, {'Content-Type': 'text/html'});
            console.log('show completed !');
            res.end(data);
        });
    });
});

app.post('/findwine', function(req, res) {
    dbConn.then(function(db) {
        var wines = db.collection('wines');
        wines.findOne(
            {_id: ObjectID(req.body._id)}, function(err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log('Find successful !');
                console.log(result);
                res.redirect('/update-wine');
                
            }
        });
    });
});

app.post('/update-wine', function(req, res) {
    dbConn.then(function(db){
        var wines = db.collection('wines');
        wines.findOne({_id: ObjectID(req.body._id)}, function(err, row){
        if (err) {
            console.log(err);
        } else {
            console.log(row);
            data = '<a href="/view-wines">Back</a>';
            data += '<h1>Update Wine</h1>';
            data += '<form action="/updateproject/'+ row._id +'" method="POST">';
            data +=    '<label>Name : <input type="text" name="name" value="' + row.name + '"></label><br>';            
            data +=    '<label>Year : <input type="text" name="year" value="' + row.year + '"></label><br>';
            data +=    '<label>Grapes : <input type="text" name="grapes" value="' + row.grapes + '"></label><br>';
            data +=    '<label>Country : <input type="text" name="country" value="' + row.country + '"></label><br>';
            data +=    '<label>Region : <input type="text" name="region" value="' + row.region + '"></label><br>';
            data +=    '<label>Description : <input type="text" name="description" value="' + row.description + '"></label><br>';
            data +=    '<label>Picture : <input type="text" name="picture" value="' + row.picture + '"></label><br>';
            data +=    '<br>';
            data +=    '<input type="submit" value="Edit Wine">';
            data +='</form><br>';
            data +='<a href="/index.html">Insert New Wine</a>';
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(data);
            }
        });
    });
});


app.post('/updateproject/:id', function(req, res) {
    dbConn.then(function(db) {
        var wines = db.collection('wines');
        var wine = req.body;
        wines.findOneAndUpdate(
            {_id: ObjectID(req.params.id)}, wine, function(err, result) {
            if (err) {
                console.log('Error updating wine: ' + err);
                res.send({'error':'An error has occurred'});
            } else {
                console.log('Updated successful !');
                res.redirect('/view-wines');
            }
        });
        
    });
});

app.post('/deleteproject', function(req, res) {
    dbConn.then(function(db) {
        var wines = db.collection('wines');
        wines.remove(
            {_id: ObjectID(req.body._id)}, function(err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log('Deleted successful !');
                res.redirect("/view-wines");
            }
        });
    });
});

